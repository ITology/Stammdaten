var express = require('express');
var router = express.Router();
var multer  = require('multer');
var sha1 = require('sha1');
var config = require('../config');
var moment = require("moment");
var is = require("is_js");
moment.locale('de-DE');

// Mit MySQL verbinden
var connection = require("./db");

UserType = {
    anonym: 0,
    schüler: 1,
    lehrer: 2,
    admin: 3
}

router.use(function(req, res, next){
    res.locals.path = req.path;
    next();
});

router.get('*', function(req, res, next) {
    sess = req.session;
    if (req.session.typ == null) {
        req.session.typ = UserType.lehrer;
        req.session.name = "Olli Kahn";
        req.session.LehrerID = 1;
    }
    if (typeof req.session.msg === "undefined") {
        req.session.msg = {};
    }
    req.session.msg = req.flash('error');

    if(req.session.typ == UserType.lehrer) {
        var benachrichtigung = require("./controller/benachrichtigung")
        benachrichtigung.get(req.session.LehrerID, function (rows) {
            rows.count = rows.length;
            req.session.benachrichtigungen = rows;
            next();
            return;
            console.log("1");
        });
    console.log("22");
    } else if(req.session.typ == UserType.admin) {
                console.log("3");
                var benachrichtigung_admin = require("./controller/benachrichtigung_admin")
                benachrichtigung_admin.get(function (rows) {
                    //res.json(nachricht);
                    rows.count = rows.length;
                    req.session.benachrichtigungen_admin = rows;
                    next();
                    return;
                });
    } else {
        next();
    }
});

router.get('/', function(req, res, next) {
    //console.log(req.session);
    res.render('index', {
        title: 'Startseite',
    });
});

router.get('/dashboard', function(req, res, next) {
    if(req.session.typ == UserType.lehrer) {
        res.render('lehrer/dashboard', {
        title: 'Dashboard',
    });
    }
    if(req.session.typ == UserType.schüler) {
        res.render('schueler/dashboard', {
            title: 'Dashboard',
        });
    }
    if(req.session.typ == UserType.admin) {
        res.render('admin/dashboard', {
            title: 'Dashboard',
        });
    }
});

router.get('/login', function(req, res, next) {
    res.render('login', {
        title: 'Anmeldung',
    });
});

router.post('/login/abschicken', function(req, res) {
    if(req.body.email == config.admin.name) {
        if(req.body.passwort == config.admin.passwort) {
            console.log("Admin-Anmeldedaten sind korrekt");
            req.session.typ = UserType.admin;
            res.redirect("/dashboard");
            return;
        }
    }

    connection.query( "SELECT * from schüler WHERE aktiv=1 AND verifiziert=1 AND Email='"+req.body.email +"'", function(err, rows, fields) {
        if (!err) {
            var nutzer = rows[0];
            if (typeof nutzer === "undefined") {
                // Es gibt keinen übereinstimmenden Datensatz, jetzt nach Lehrern suchen
                connection.query("SELECT * from lehrer WHERE aktiv=1 AND Email='"+req.body.email +"'", function(err, rows, fields) {
                    var nutzer = rows[0];
                    // Es gibt keinen übereinstimmenden Datensatz, Fehler ausgeben
                    if(typeof nutzer === "undefined") {
                        req.flash("error", "Nutzer gibt es nicht, oder wurde noch nicht verifiziert!");
                        res.redirect("/login");
                        return;
                    }
                    if(sha1(req.body.passwort) == nutzer["PasswortHash"]) {
                        connection.query("SELECT *, ID as KlassenID from klasse WHERE Klassenlehrer_ID=?", [nutzer.ID], function(err, klasse, fields) {
                            var daten = klasse[0];
                            if(typeof daten == "undefined") {
                                req.session.Klasse_ID = null;
                            }
                            else {
                                req.session.Klasse_ID = daten.KlassenID;
                            }
                        sess = req.session;
                        req.session.typ = UserType.lehrer;
                        req.session.name = nutzer["Vorname"] + " " + nutzer["Nachname"];
                        req.session.LehrerID = nutzer["ID"];
                            console.log(req.session.Klasse_ID);
                        //console.log(sess);
                        console.log("Lehrer " + nutzer["Vorname"] + " " + nutzer["Nachname"] + " angemeldet!");
                            res.redirect("/dashboard");
                        });
                    } else {
                        req.flash("error", "Falsches Passwort!");
                        res.redirect("/");
                    }
                }
            )} else {
                // Schaue, ob Passwort übereinstimmt
                if(sha1(req.body.passwort) == nutzer["PasswortHash"]) {
                    sess = req.session;
                    req.session.typ = UserType.schüler;
                    req.session.name = nutzer["Vorname"] + " " + nutzer["Nachname"];
                    req.session.SchülerID = nutzer["ID"];
                    //console.log(sess);
                    console.log("Schüler " + nutzer["Vorname"] + " " + nutzer["Nachname"] + " angemeldet!");
                    res.redirect("/dashboard");
                } else {
                    req.flash("error", "Falsches Passwort!");
                    res.redirect("/");
                }
            }
        } else {
            console.log('Error while performing Query.');
        }
    });
    //res.send("Email: " + req.body.email + "  Pw: " + req.body.email);
});

router.get('/registrieren', function(req, res, next) {
    connection.query("SELECT * FROM ausbildungsberuf", function(err, rows, fields) {
        connection.query("SELECT * FROM klasse", function(err, klasse, fields) {
            if (err) throw(err);
            res.render('register', {
                title: 'Registrieren',
                ausbildungsberufe: rows,
                klasse: klasse,
                path: req.path,
            });
            console.log(klasse);
        });
    });
});

router.post('/registrieren/abschicken',function(req, res, next){
    //Auslesen aus der Registrierung
    var schüler = {
        EMail: req.body.EMail,
        PasswortHash: sha1(req.body.PasswortHash),
        Anrede: req.body.Anrede,
        Vorname: req.body.Vorname,
        Nachname: req.body.Nachname,
        Geburtsdatum: req.body.Geburtsdatum,
        Geburtsort: req.body.Geburtsort,
        Telefonnummer: req.body.Telefonnummer,
        Klasse_ID: req.body.Klasse_Name,
        Handynummer: req.body.Handynummer,
        Religion: req.body.Religion,
        Staatsangehörigkeit: req.body.Staatsangehörigkeit,
        Flüchtling: req.body.Flüchtling,
        Einwanderer: req.body.Einwanderer,
        Einwanderer_seit: null,
        Zuzug: req.body.Zuzug,
        Familiensprache: req.body.Familiensprache,
        Abschluss_vorher: req.body.Abschluss_vorher,
        Eheleute: req.body.Eheleute,
        Vater_Vorname: req.body.Vater_Vorname,
        Vater_Name: req.body.Vater_Name,
        Mutter_Vorname: req.body.Mutter_Vorname,
        Mutter_Name: req.body.Mutter_Name,
        Ansprechpartner_1_Vorname: req.body.Ansprechpartner_1_Vorname,
        Ansprechpartner_1_Name: req.body.Ansprechpartner_1_Name,
        Ansprechpartner_1_Telefonnummer: req.body.Ansprechpartner_1_Telefonnummer,
        Ansprechpartner_2_Vorname: req.body.Ansprechpartner_2_Vorname,
        Ansprechpartner_2_Name: req.body.Ansprechpartner_2_Name,
        Ansprechpartner_2_Telefonnummer: req.body.Ansprechpartner_2_Telefonnummer,
        Schule_vorher: req.body.Schule_vorher,
        Schule_vorher_Ort: req.body.Schule_vorher_Ort,
        Wohnort: req.body.Wohnort,
        PLZ: req.body.PLZ,
        Straße_vorher: req.body.Straße_vorher,
        Straße: req.body.Straße,
        Hausnummer: req.body.Hausnummer,
        Ausbildungsberuf_ID: req.body.Ausbildungsberuf_Name,
        Ausbildungsbetrieb_ID: null,
    }

    if(req.body.Einwanderer == "1") {
        schüler.Einwanderer_seit = req.body.Einwanderer_seit;
    }
    console.log(schüler)

    var betrieb = {
        Ausbildungsbetrieb_Name: req.body.Ausbildungsbetrieb_Name,
        Ausbildungsbetrieb_PLZ: req.body.Ausbildungsbetrieb_PLZ,
        Ausbildungsbetrieb_Straße: req.body.Ausbildungsbetrieb_Straße,
        Ausbildungsbetrieb_Ort: req.body.Ausbildungsbetrieb_Ort,
        Ausbildungsbetrieb_Hausnummer: req.body.Ausbildungsbetrieb_Hausnummer,
        Ausbildungsbetrieb_Telefonnummer: req.body.Ausbildungsbetrieb_Telefonnummer,
        Ausbildungsbetrieb_EMail: req.body.Ausbildungsbetrieb_EMail,
        Ansprechpartner: req.body.Ansprechpartner,
    }

    connection.query("SELECT ID, Ausbildungsbetrieb_Name FROM ausbildungsbetrieb WHERE Ausbildungsbetrieb_Name=?", [req.body.Ausbildungsbetrieb_Name], function(err, rows, fields) {
        if (err) throw(err);
        if (typeof rows[0] === "undefined") {
            // Ausbildungsbetrieb gibt es bis jetzt nicht
            // Ausbildungsbetrieb jetzt neu anlegen
            connection.query("INSERT INTO ausbildungsbetrieb SET ?", [betrieb], function(err, result) {
                if (err) throw err;
                schüler.Ausbildungsbetrieb_ID = result.insertId;
                connection.query("INSERT INTO schüler SET ?", [schüler], function(err, result) {
                    if (err) throw err;
                    Schüler_ResultID = result.insertId;
                    connection.query('SELECT *, lehrer.Vorname AS Lehrer_Vorname, lehrer.ID AS LehrerID FROM schüler INNER JOIN ausbildungsbetrieb ON schüler.Ausbildungsbetrieb_ID = ausbildungsbetrieb.ID INNER JOIN ausbildungsberuf ON schüler.Ausbildungsberuf_ID = ausbildungsberuf.Ausbildungsberuf_ID INNER JOIN klasse ON schüler.Klasse_ID = klasse.ID INNER JOIN lehrer on klasse.Klassenlehrer_ID = Lehrer.ID WHERE schüler.ID=?', [Schüler_ResultID], function(err, rows, fields) {
                        if (err) throw err;
                        nachricht_ = rows[0];
                            connection.query("SELECT * FROM klasse", function(err, klasse, fields) {
                                if (err) throw(err);
                                connection.query("SELECT * FROM ausbildungsberuf", function(err, ausbildungsberufe, fields) {
                                    if (err) throw(err);
                                        res.render('schueler/benachrichtigung', {
                                            title: 'Benachrichtigung',
                                            daten: rows[0],
                                            klasse: klasse,
                                            ausbildungsberufe: ausbildungsberufe,
                                        });
                                });
                            });
                            var benachrichtigung = require("./controller/benachrichtigung");
                            benachrichtigung.add(nachricht_.LehrerID, Schüler_ResultID, "Bitte verifizieren.");
                        });
                });
            });
        //Falls Ausbildungsbetrieb bereits vorhanden
        } else {
            console.log("test");
            schüler.Ausbildungsbetrieb_ID = rows[0].ID;
            connection.query("INSERT INTO schüler SET ?", [schüler], function(err, result) {
                if (err) throw err;
                Schüler_ResultID = result.insertId;
                connection.query('SELECT *, lehrer.Vorname AS Lehrer_Vorname, lehrer.ID AS LehrerID FROM schüler INNER JOIN ausbildungsbetrieb ON schüler.Ausbildungsbetrieb_ID = ausbildungsbetrieb.ID INNER JOIN ausbildungsberuf ON schüler.Ausbildungsberuf_ID = ausbildungsberuf.Ausbildungsberuf_ID INNER JOIN klasse ON schüler.Klasse_ID = klasse.ID INNER JOIN lehrer on klasse.Klassenlehrer_ID = Lehrer.ID WHERE schüler.ID=?', [Schüler_ResultID], function(err, rows, fields) {
                    if (err) throw err;
                    nachricht_ = rows[0];
                        connection.query("SELECT * FROM klasse", function(err, klasse, fields) {
                            if (err) throw(err);
                            connection.query("SELECT * FROM ausbildungsberuf", function(err, ausbildungsberufe, fields) {
                                if (err) throw(err);
                                    res.render('schueler/benachrichtigung', {
                                        title: 'Benachrichtigung',
                                        daten: rows[0],
                                        klasse: klasse,
                                        ausbildungsberufe: ausbildungsberufe,
                                    });
                            });
                        })
                        var benachrichtigung = require("./controller/benachrichtigung");
                        benachrichtigung.add(nachricht_.LehrerID, Schüler_ResultID, "Bitte verifizieren.");
                    });
            });
        }
        });
    res.redirect("/login");
});

// ----------------------
// Seiten für den Schüler
// ----------------------
router.get('/schueler/benachrichtigung', function(req, res, next) {
    if(req.session.typ == UserType.schüler) {
        connection.query('SELECT *, lehrer.Vorname AS Lehrer_Vorname, lehrer.ID AS LehrerID FROM schüler INNER JOIN ausbildungsbetrieb ON schüler.Ausbildungsbetrieb_ID = ausbildungsbetrieb.ID INNER JOIN ausbildungsberuf ON schüler.Ausbildungsberuf_ID = ausbildungsberuf.Ausbildungsberuf_ID INNER JOIN klasse ON schüler.Klasse_ID = klasse.ID INNER JOIN lehrer on klasse.Klassenlehrer_ID = Lehrer.ID WHERE schüler.ID=?', [req.session.SchülerID], function(err, rows, fields) {
            if (err) throw err;
                connection.query("SELECT * FROM klasse", function(err, klasse, fields) {
                    if (err) throw(err);
                    connection.query("SELECT * FROM ausbildungsberuf", function(err, ausbildungsberufe, fields) {
                        if (err) throw(err);
                            res.render('schueler/benachrichtigung', {
                                title: 'Benachrichtigung',
                                daten: rows[0],
                                klasse: klasse,
                                ausbildungsberufe: ausbildungsberufe,
                        });
                    });
                });
            });
    }
});

router.post('/schueler/stammdaten/abschicken/:id', function(req, res, next) {
    var schüler = {
        verifiziert: req.body.verifiziert,
        EMail: req.body.EMail,
        //PasswortHash: sha1(req.body.PasswortHash),
        Anrede: req.body.Anrede,
        Vorname: req.body.Vorname,
        Nachname: req.body.Nachname,
        Geburtsdatum: req.body.Geburtsdatum,
        Geburtsort: req.body.Geburtsort,
        Telefonnummer: req.body.Telefonnummer,
        //Klasse_ID: req.body.Klasse_Name,
        Handynummer: req.body.Handynummer,
        Religion: req.body.Religion,
        Staatsangehörigkeit: req.body.Staatsangehörigkeit,
        Flüchtling: req.body.Flüchtling,
        Einwanderer: req.body.Einwanderer,
        Einwanderer_seit: null,
        Zuzug: req.body.Zuzug,
        Familiensprache: req.body.Familiensprache,
        Abschluss_vorher: req.body.Abschluss_vorher,
        Eheleute: req.body.Eheleute,
        Vater_Vorname: req.body.Vater_Vorname,
        Vater_Name: req.body.Vater_Name,
        Mutter_Vorname: req.body.Mutter_Vorname,
        Mutter_Name: req.body.Mutter_Name,
        Ansprechpartner_1_Vorname: req.body.Ansprechpartner_1_Vorname,
        Ansprechpartner_1_Name: req.body.Ansprechpartner_1_Name,
        Ansprechpartner_1_Telefonnummer: req.body.Ansprechpartner_1_Telefonnummer,
        Ansprechpartner_2_Vorname: req.body.Ansprechpartner_2_Vorname,
        Ansprechpartner_2_Name: req.body.Ansprechpartner_2_Name,
        Ansprechpartner_2_Telefonnummer: req.body.Ansprechpartner_2_Telefonnummer,
        Schule_vorher: req.body.Schule_vorher,
        Schule_vorher_Ort: req.body.Schule_vorher_Ort,
        Wohnort: req.body.Wohnort,
        PLZ: req.body.PLZ,
        Straße_vorher: req.body.Straße_vorher,
        Straße: req.body.Straße,
        Hausnummer: req.body.Hausnummer,
        Ausbildungsberuf_ID: req.body.Ausbildungsberuf_Name,
        Ausbildungsbetrieb_ID: null,
        Formular1: req.body.Formular1,
        Formular2: req.body.Formular2,
        Formular3: req.body.Formular3,
        Formular4: req.body.Formular4,
        Formular5: req.body.Formular5,
    }
    if(req.body.Einwanderer == "1") {
        schüler.Einwanderer_seit = req.body.Einwanderer_seit;
    }

    var betrieb = {
        Ausbildungsbetrieb_Name: req.body.Ausbildungsbetrieb_Name,
        Ausbildungsbetrieb_PLZ: req.body.Ausbildungsbetrieb_PLZ,
        Ausbildungsbetrieb_Straße: req.body.Ausbildungsbetrieb_Straße,
        Ausbildungsbetrieb_Ort: req.body.Ausbildungsbetrieb_Ort,
        Ausbildungsbetrieb_Hausnummer: req.body.Ausbildungsbetrieb_Hausnummer,
        Ausbildungsbetrieb_Telefonnummer: req.body.Ausbildungsbetrieb_Telefonnummer,
        Ausbildungsbetrieb_EMail: req.body.Ausbildungsbetrieb_EMail,
        Ansprechpartner: req.body.Ansprechpartner,
    }
    connection.query("SELECT ID, Ausbildungsbetrieb_Name FROM ausbildungsbetrieb WHERE Ausbildungsbetrieb_Name=?", [req.body.Ausbildungsbetrieb_Name], function(err, rows, fields) {
        if (err) throw(err);
        if (typeof rows[0] === "undefined") {
            // Ausbildungsbetrieb gibt es bis jetzt nicht
            // Ausbildungsbetrieb jetzt neu anlegen
            connection.query("INSERT INTO ausbildungsbetrieb SET ?", [betrieb], function(err, result) {
                if (err) throw err;
                schüler.Ausbildungsbetrieb_ID = result.insertId;
                connection.query("UPDATE schüler SET ? WHERE schüler.ID=?", [schüler,req.session.SchülerID], function(err, result) {
                    if (err) throw err;
                });
            });
        //Falls Ausbildungsbetrieb bereits vorhanden
        } else {
            if(req.session.typ == UserType.lehrer) {
                schüler.Ausbildungsbetrieb_ID = rows[0].ID;
                var sql = connection.query("UPDATE schüler SET ? WHERE ID=?", [schüler, req.params.id], function(err, result) {
                    if (err) throw err;
                });
                console.log(sql.sql);
            }
        }
    });
    if(req.session.typ == UserType.schüler) {
        res.redirect("/schueler/stammdaten");
    } else {
    res.redirect("/klasse");
}
});

router.get('/schueler/formulare', function(req, res, next) {

});

router.get("/schueler/formulare/download/:id", function(req, res, next) {
    connection.query("SELECT * FROM formular WHERE ID="+req.params.id, function(err, rows, fields) {
        if (err) throw err;
        if (typeof rows[0] === "undefined") {
            req.flash("error", "Formular existiert nicht!");
            if(req.session.typ == UserType.schüler) {
            res.redirect("/schueler/formulare");
            } else if(req.session.typ == UserType.lehrer){
                res.redirect("/formulare");
            }
        } else {
            if(rows[0].Formular == null) {
                res.redirect("/schueler/formulare");
            } else {
                res.setHeader('Content-disposition', 'attachment; filename='+rows[0].Dateiname);
                res.send(rows[0].Formular);
            }
        }
    });
});

router.get("/schueler/formulare/view/:id", function(req, res, next) {
    connection.query("SELECT * FROM formular WHERE ID="+req.params.id, function(err, rows, fields) {
        if (err) throw err;
        if (typeof rows[0] === "undefined") {
            res.redirect("/schueler/formulare");
        } else {
            if(rows[0].Formular == null) {
                res.redirect("/schueler/formulare");
            } else {
                res.setHeader('Content-Type', 'image/jpeg');
                res.send(rows[0].Formular);
            }
        }
    });
});

router.post("/schueler/nachricht/abschicken", function(req, res, next) {
    var benachrichtigung = require("./controller/benachrichtigung")
    benachrichtigung.add(req.body.LehrerID, req.session.SchülerID, req.body.Anfrage);
    req.flash("error", "Nachricht erfolgreicht verschickt!")
    res.redirect("/stammdaten");
});

// ----------------------
// Seiten für den Lehrer
// ----------------------

router.get('/klasse', function(req, res, next) {
    if(req.session.typ == UserType.lehrer) {
        connection.query("SELECT schüler.ID, schüler.Vorname, schüler.Nachname, schüler.EMail FROM schüler INNER JOIN klasse ON schüler.Klasse_ID = klasse.ID INNER JOIN lehrer ON klasse.Klassenlehrer_ID = lehrer.ID WHERE lehrer.ID =?",[req.session.LehrerID]
        , function(err, rows, fields) {
            res.render('lehrer/klasse', {
                schülers: rows,
                title: 'Klasse',
            });
        });
    } else {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
    }
});

router.get('/klasse/schueler/:id', function(req, res, next) {
    if(req.session.typ >= UserType.lehrer) {
        var yolo = connection.query('SELECT *,schüler.ID AS schüler_ID FROM schüler INNER JOIN ausbildungsbetrieb ON schüler.Ausbildungsbetrieb_ID = ausbildungsbetrieb.ID INNER JOIN ausbildungsberuf ON schüler.Ausbildungsberuf_ID = ausbildungsberuf.Ausbildungsberuf_ID INNER JOIN klasse ON schüler.Klasse_ID = klasse.ID WHERE schüler.ID=?', [req.params.id], function(err, rows, fields) {
            if (err) throw err;
                connection.query("SELECT * FROM klasse", function(err, klasse, fields) {
                    if (err) throw(err);
                    connection.query("SELECT * FROM ausbildungsberuf", function(err, ausbildungsberufe, fields) {
                        if (err) throw(err);
                            rows[0].Geburtsdatum = moment(rows[0].Geburtsdatum).format("YYYY-MM-DD")
                            rows[0].Einwanderer_seit = moment(rows[0].Einwanderer_seit).format("YYYY-MM-DD")
                            rows[0].Zuzug = moment(rows[0].Zuzug).format("YYYY-MM-DD")
                            res.render('schueler/stammdaten', {
                                title: 'Stammdaten',
                                daten: rows[0],
                                klasse: klasse,
                                ausbildungsberufe: ausbildungsberufe,
                            });
                        });
                    });
                });
        console.log(yolo.sql);
    } else {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
    }
});

router.get("/klasse/schueler/:id/delete", function(req, res, next) {
    connection.query("DELETE FROM schüler WHERE ID=?", [req.params.id], function(err, rows, fields) {
        if (err) throw err;
        res.redirect("/klasse")
    });
});

router.get('/formulare', function(req, res, next) {
    if(req.session.typ == UserType.schüler) {
        connection.query('SELECT * FROM formular', function(err, rows, fields) {
            if (err) throw err;
            console.log(rows);
            res.render('schueler/formulare', {
                formulare: rows,
                title: 'Formulare',
            });
        });
    }
    if(req.session.typ >= UserType.lehrer) {
        connection.query('SELECT *,UNIX_TIMESTAMP(Datum) AS Datum_Unix FROM formular', function(err, rows, fields) {
            if (err) throw err;
            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                var d = moment.unix(row.Datum_Unix)
                row["DatumFormat"] = d.format('LLL');
            }
            res.render('lehrer/formulare', {
                formulare: rows,
                title: 'Formulare',
            });
        });
    }
});

router.get('/formulare/:id/edit', function(req, res, next) {
    if(req.session.typ <= UserType.schüler) {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
        return;
    }
    if(req.params.id == "new") {
        var formular = {
            Name: "",
            Beschreibung: "",
            Dateiname: "",
            Formular: null,
        }
        res.render('lehrer/formular_edit', {
            formular: formular,
            title: 'Formulare',
        });
    } else {
        connection.query('SELECT *,UNIX_TIMESTAMP(Datum) AS Datum_Unix FROM formular WHERE ID=?', [req.params.id], function(err, rows, fields) {
            if (err) throw err;
            res.render('lehrer/formular_edit', {
                formular: rows[0],
                title: 'Formulare',
            });
        });
    }
});

router.get("/formulare/:id/delete", function(req, res, next) {
    if(req.session.typ <= UserType.schueler) {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
        return;
    }
    connection.query("DELETE FROM formular WHERE ID=?", [req.params.id], function(err, rows, fields) {
        if (err) throw err;
        switch(req.session.typ) {
        case UserType.lehrer:
            res.redirect("/formulare");
            break;
        case UserType.admin:
            res.redirect("/admin/formulare");
            break;
        default:
            res.redirect("/");
        }
    });
});

router.use(multer({
    dest: './upload',
    inMemory: true,
    onFileUploadStart: function(file) {
        if(file.mimetype !== 'image/jpg' && file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png') {
            // TODO: Fehlermeldung abschicken
            return false;
        }
    },
    onFileUploadComplete: function (file, req, res) {
        if(req.body.ID == "undefined") {
            // TODO: Error abfangen
            var data = {
                Name: req.body.Name,
                Beschreibung: req.body.Beschreibung,
                Dateiname: req.files.Formular[0].originalname,
                Formular: req.files.Formular[0].buffer,
            }
            connection.query("INSERT INTO formular SET ?, Datum=NOW()", [data], function(err, rows, fields) {
                if (err) throw err;
            });
            return;
        } else {
        console.log(req.body);
        console.log(req.files.Formular[0]);
        var data = {
            Name: req.body.Name,
            Beschreibung: req.body.Beschreibung,
            Dateiname: req.files.Formular[0].originalname,
            Formular: req.files.Formular[0].buffer,
            Datum: moment().valueOf(),
        }
        console.log(data);
            connection.query("UPDATE formular SET ? WHERE ID=?", [data, req.body.ID], function(err, rows, fields) {
            if (err) throw err;
        });
        console.log("Formular-Daten wurden geändert, aber BESONDERS die Datei!")
    }
    }
}));

router.post("/formular/post", function(req, res){
    if(!(req.session.typ > UserType.schüler)) {
        res.redirect("/");
        return;
    }
    console.log("ROUTER.POST");
    console.log(req.body);
    if (typeof(req.body.Formular) == "undefined") {
        console.log("Formular-Daten wurden geändert, aber nicht die Datei!")
        var data = {
            Name: req.body.Name,
            Beschreibung: req.body.Beschreibung,
        }
        console.log(data);
        connection.query("UPDATE formular SET ?, Datum=NOW() WHERE ID=?", [data, req.body.ID]);
        //connection.query("UPDATE formular SET Datum = NOW() WHERE ID=?",id);
    };
    res.redirect("/formulare");
});

router.get('/betriebe', function(req, res, next) {
    if(req.session.typ >= UserType.lehrer) {
        connection.query("SELECT ID, Ausbildungsbetrieb_Name, Ausbildungsbetrieb_Ort, Ausbildungsbetrieb_PLZ, Ausbildungsbetrieb_Telefonnummer FROM ausbildungsbetrieb"
        , function(err, rows, fields) {
            if (err) throw err;
            console.log(rows);
            res.render('lehrer/betriebe', {
                betriebe: rows,
                title: 'Betriebe',
            });
        });
    } else {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
    }
});

router.get('/betrieb/:id', function(req, res, next) {
    if(req.session.typ >= UserType.lehrer) {
        connection.query('SELECT * FROM ausbildungsbetrieb WHERE ID=?', [req.params.id], function(err, rows, fields) {
            if (err) throw err;
            res.render('lehrer/betrieb', {
                title: 'Betriebe',
                betriebe: rows[0],
            });
        });
    }
});

router.post('/betrieb/:id/abschicken', function(req, res, next) {

    var betrieb = {
        Ausbildungsbetrieb_Name: req.body.Ausbildungsbetrieb_Name,
        Ausbildungsbetrieb_PLZ: req.body.Ausbildungsbetrieb_PLZ,
        Ausbildungsbetrieb_Straße: req.body.Ausbildungsbetrieb_Straße,
        Ausbildungsbetrieb_Ort: req.body.Ausbildungsbetrieb_Ort,
        Ausbildungsbetrieb_Hausnummer: req.body.Ausbildungsbetrieb_Hausnummer,
        Ausbildungsbetrieb_Telefonnummer: req.body.Ausbildungsbetrieb_Telefonnummer,
        Ausbildungsbetrieb_EMail: req.body.Ausbildungsbetrieb_EMail,
    }
    console.log(betrieb);
    if(req.session.typ >= UserType.lehrer) {
        connection.query('UPDATE ausbildungsbetrieb SET ? WHERE ausbildungsbetrieb.ID=?', [betrieb, req.params.id], function(err, rows, fields) {
            if (err) throw err;
            res.redirect("/betriebe")
        });
    }
});

router.get("/betriebe/:id/delete", function(req, res, next) {
    console.log(req.params);
    connection.query("DELETE FROM ausbildungsbetrieb WHERE ID=?", [req.params.id], function(err, rows, fields) {
        if (err) {
            req.flash("error", "Betrieb kann nicht gelöscht werden, da er noch mit einem Schüler verknüpft ist.");
        }
        res.redirect("/betriebe")
    });
});
router.get("/schuelersuche/", function(req, res, next) {
    connection.query("SELECT ID, Vorname, Nachname, EMail FROM schüler", function(err, rows, fields) {
        if (err) throw err;
        res.render('lehrer/schüler', {
            title: 'Schülersuche',
            schülers: rows,
        });
    });
});
router.get("/lehrersuche/", function(req, res, next) {
    var suche =
    connection.query("SELECT ID, Vorname, Nachname, EMail FROM lehrer", function(err, rows, fields) {
        if (err) throw err;
        console.log(rows.sql);
        res.render('admin/lehrersuche', {
            title: 'Lehrersuche',
            lehrer: rows,
        });
    });
});
router.get('/stammdaten', function(req, res, next) {
    if(req.session.typ == UserType.schüler) {
        console.log(test);
        connection.query('SELECT * FROM schüler INNER JOIN ausbildungsbetrieb ON schüler.Ausbildungsbetrieb_ID = ausbildungsbetrieb.ID INNER JOIN ausbildungsberuf ON schüler.Ausbildungsberuf_ID = ausbildungsberuf.Ausbildungsberuf_ID INNER JOIN klasse ON schüler.Klasse_ID = klasse.ID WHERE schüler.ID=?', [req.session.SchülerID], function(err, rows, fields) {
            if (err) throw err;
                connection.query("SELECT * FROM klasse", function(err, klasse, fields) {
                    if (err) throw(err);
                    connection.query("SELECT * FROM ausbildungsberuf", function(err, ausbildungsberufe, fields) {
                        if (err) throw(err);
                    rows[0].Geburtsdatum = moment(rows[0].Geburtsdatum).format("YYYY-MM-DD")
                    rows[0].Einwanderer_seit = moment(rows[0].Einwanderer_seit).format("YYYY-MM-DD")
                    rows[0].Zuzug = moment(rows[0].Zuzug).format("YYYY-MM-DD")
                            res.render('schueler/stammdaten', {
                                title: 'Stammdaten',
                                daten: rows[0],
                                klasse: klasse,
                                ausbildungsberufe: ausbildungsberufe,
                            });
                        });
                    });
                });
    }
    else if(req.session.typ == UserType.lehrer) {
        var test = connection.query('SELECT * FROM lehrer WHERE ID=?', [req.session.LehrerID], function(err, rows, fields) {
            connection.query('SELECT * FROM klasse WHERE Klassenlehrer_ID=?', [req.session.LehrerID], function(err, klasse, fields) {
            if (err) throw err;
                res.render('lehrer/stammdaten', {
                    lehrer: rows[0],
                    klasse: klasse[0],
                    title: 'Stammdaten',
                })
        });
        console.log("test");
        console.log(test.sql);
    });
    }
    else {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
    }
});
router.post('/stammdaten/abschicken/:id', function(req, res, next) {
    if(req.session.typ == UserType.admin)
    {
        var lehrer = {
            Klasse_ID: req.body.Klasse_ID,
            EMail: req.body.Lehrer_EMail,
            //PasswortHash: req.body.Lehrer_PasswortHash,
            Vorname: req.body.Lehrer_Vorname,
            Nachname: req.body.Lehrer_Nachname,
            Anrede: req.body.Lehrer_Anrede,
            Geburtsdatum: req.body.Lehrer_Geburtsdatum,
            Telefonnummer: req.body.Lehrer_Telefonnummer,
            Handynummer: req.body.Lehrer_Handynummer,
            Wohnort: req.body.Lehrer_wohnort,
            PLZ: req.body.Lehrer_plz,
            Straße: req.body.Lehrer_straße,
            Hausnummer: req.body.Lehrer_hausnummer,
        }
    } else {
        var lehrer = {
            EMail: req.body.Lehrer_EMail,
            //PasswortHash: req.body.Lehrer_PasswortHash,
            Vorname: req.body.Lehrer_Vorname,
            Nachname: req.body.Lehrer_Nachname,
            Anrede: req.body.Lehrer_Anrede,
            Geburtsdatum: req.body.Lehrer_Geburtsdatum,
            Telefonnummer: req.body.Lehrer_Telefonnummer,
            Handynummer: req.body.Lehrer_Handynummer,
            Wohnort: req.body.Lehrer_wohnort,
            PLZ: req.body.Lehrer_plz,
            Straße: req.body.Lehrer_straße,
            Hausnummer: req.body.Lehrer_hausnummer,
        }
    }
    if(req.session.typ >= UserType.lehrer) {
        connection.query("UPDATE lehrer SET ? WHERE lehrer.ID=?", [lehrer, req.session.LehrerID], function(err, rows, fields) {
            if (err) throw(err);
            res.redirect("/stammdaten")
        });
    }
});

router.get("/admin/lehrer/:id/delete", function(req, res, next) {
    connection.query("DELETE FROM schüler WHERE ID=?", [req.params.id], function(err, rows, fields) {
        if (err) throw err;
        res.redirect("/lehrer/lehrersuche")
    });
});

router.get("/lehrer/lehrersuche/:id", function(req, res, next) {
    if(req.session.typ >= UserType.schüler) {
        var lehrer = connection.query('SELECT * FROM lehrer WHERE lehrer.ID=?', [req.params.id], function(err, rows, fields) {
                connection.query('SELECT * FROM klasse WHERE Klassenlehrer_ID=?', [req.params.id], function(err, klasse, fields) {
                    if (err) throw err;
                    if(klasse.length == 0) {
                        console.log("lehrer ist leer!")
                        klasse[0] = {
                            ID: "",
                            Name: "",
                            Klassenlehrer_ID: null,
                        }
                    }
                    console.log(rows)
                    console.log(klasse)
                    rows[0].Geburtsdatum = moment(rows[0].Geburtsdatum).format("YYYY-MM-DD")
                    res.render('lehrer/stammdaten', {
                        title: 'Stammdaten',
                        lehrer: rows[0],
                        klasse: klasse[0],
                    });
            });
    });
    } else {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
    }
});

router.get("/schulbesuch", function(req, res, next) {
    res.render('lehrer/schulbesuch', {
        title: 'Schulbesuch',
    });
});
router.get("/nachricht/:id", function (req, res, next) {
    var benachrichtigung = require("./controller/benachrichtigung")
    benachrichtigung.getNachricht(req.params.id, function (nachricht) {
        //res.json(nachricht);
        res.render('lehrer/lehrer_benachrichtigung', {
            title: 'Benachrichtigung',
            nachricht: nachricht,
        });
    });
});

router.get("/nachricht/:id/delete", function(req, res, next) {
    connection.query("DELETE FROM benachrichtigungen WHERE ID=?",[req.params.id], function(err, rows, fields) {
        if (err) throw err;
        res.redirect("/klasse")
    });
});
// --------------------------------------------
// Seiten für Admin
// --------------------------------------------

router.get('/ausbildungsberufe', function(req, res, next) {
    if(req.session.typ != UserType.admin) {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
        return;
    }
    connection.query("SELECT * FROM ausbildungsberuf", function(err, rows, fields) {
        if (err) throw err;
        res.render('admin/ausbildungsberufe', {
            title: 'Ausbildungsberufe',
            ausbildungsberufe: rows,
        });
    });
});

router.get('/ausbildungsberufe/:id/edit', function(req, res, next) {
    if(req.session.typ != UserType.admin) {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
        return;
    }
    if(req.params.id == "new") {
        var ausbildungsberuf = {
            Ausbildungsberuf_ID: "new",
            Ausbildungsberuf_Name: "",
            Fachrichtung: "",
        }
        res.render('admin/ausbildungsberuf_edit', {
            ausbildungsberuf: ausbildungsberuf,
            title: 'Ausbildungsberuf',
        });
    } else {
        connection.query("SELECT * FROM ausbildungsberuf WHERE Ausbildungsberuf_ID=?", [req.params.id], function(err, rows, fields) {
            res.render('admin/ausbildungsberuf_edit', {
                ausbildungsberuf: rows[0],
                title: 'Klassen',
            });
        });
    }
});

router.post("/ausbildungsberufe/:id/abschicken", function(req, res, next) {
    var daten = {
        Ausbildungsberuf_Name: req.body.Ausbildungsberuf_Name,
        Fachrichtung: req.body.Fachrichtung,
    }
    console.log(req.body)
    console.log(daten)
    if(req.params.id == "new") {
        connection.query("INSERT INTO ausbildungsberuf SET ?",[daten], function(err, result) {
            if(err) throw err;
        });
    } else {
        connection.query("UPDATE ausbildungsberuf SET ? WHERE Ausbildungsberuf_ID=?",[daten, req.params.id], function(err, result) {
            if(err) throw err;
        });
    }
    res.redirect("/ausbildungsberufe");
});

router.get("/ausbildungsberufe/:id/delete", function(req, res, next) {
    connection.query("DELETE FROM ausbildungsberuf WHERE Ausbildungsberuf_ID=?", [req.params.id], function(err, rows, fields) {
        if (err) throw err;
        res.redirect("/ausbildungsberufe");
    });
});

router.get('/klassen', function(req, res, next) {
    if(req.session.typ != UserType.admin) {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
        return;
    }
    connection.query("SELECT klasse.*, lehrer.Vorname, lehrer.Nachname, lehrer.ID AS lehrer_ID FROM klasse " +
        "INNER JOIN lehrer ON lehrer.ID = klasse.Klassenlehrer_ID"
        , function(err, rows, fields) {
        if (err) throw err;
        res.render('admin/klassen', {
            klassen: rows,
            title: 'Klassen',
        });
    });
});
router.get('/klassen/:id/edit', function(req, res, next) {
    if(req.session.typ != UserType.admin) {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
        return;
    }
    if(req.params.id == "new") {
        connection.query("SELECT * FROM lehrer", [req.params.id], function(err, lehrer, fields) {
            var klasse = {
                ID: "new",
                Name: "",
                Klassenlehrer: "",
            }
            res.render('admin/klasse_edit', {
                klasse: klasse,
                lehrer:  lehrer,
                title: 'Klassen',
            });
        });
    } else {
        connection.query("SELECT * FROM klasse WHERE ID=?", [req.params.id], function(err, klassen, fields) {
            if (err) throw err;
            connection.query("SELECT * FROM lehrer", [req.params.id], function(err, lehrer, fields) {
                res.render('admin/klasse_edit', {
                    klasse: klassen[0],
                    lehrer:  lehrer,
                    title: 'Klassen',
                });
            });
        });
    }
});

router.post("/klassen/:id/abschicken", function(req, res, next) {
    var daten = {
        Name: req.body.Name,
        Klassenlehrer_ID: req.body.Klassenlehrer_ID,
    }
    if(req.params.id == "new") {
        connection.query("INSERT INTO klasse SET ?",[daten], function(err, result) {
            if(err) throw err;
        });
    } else {
        connection.query("UPDATE klasse SET ? WHERE ID=?",[daten, req.params.id], function(err, result) {
            if(err) throw err;
        });
    }
    res.redirect("/klassen/");
});

router.get("/klassen/:id/delete", function(req, res, next) {
    connection.query("DELETE FROM klasse WHERE ID=?", [req.params.id], function(err, rows, fields) {
        if (err) {
            req.flash("error", "Löschen fehlgeschlagen: Es sind noch Schüler in der Klasse!");
            res.redirect("/klassen")
        } else {
            res.redirect("/klassen")
        }
    });
});

router.get("/schueler/pdf/:id", function(req, res, next) {
    var PDFDocument = require("pdfkit");
    var doc = new PDFDocument();
    var fs = require("fs");
    var is = require("is_js");

    var daten = null;
    connection.query('SELECT * FROM schüler INNER JOIN ausbildungsbetrieb ON schüler.Ausbildungsbetrieb_ID = ausbildungsbetrieb.ID INNER JOIN ausbildungsberuf ON schüler.Ausbildungsberuf_ID = ausbildungsberuf.Ausbildungsberuf_ID INNER JOIN klasse ON schüler.Klasse_ID = klasse.ID WHERE schüler.ID=?', [req.params.id], function(err, rows, fields) {
        if (err) throw err;
        daten = rows[0];
        // Abfangen, wenn Datzensatz nicht existiert
        if(typeof daten == "undefined") {
            res.send("Fehler!");
            return;
        }
        var linkeSpalte = "";
        var rechteSpalte = "";

        function out(Name, Wert, bool) {
            if(is.not.existy(Wert)) return;
            if(typeof bool !== "undefined") {
                if(Wert == "0") {
                    Wert = "Nein"
                } else if(Wert == "1") {
                    Wert = "Ja"
                }
            }
            linkeSpalte += Name + "\n"
            rechteSpalte += Wert + "\n"
        }

        out("Anrede", daten.Anrede)
        out("Vorname", daten.Vorname)
        out("Nachname", daten.Nachname)
        out("Geburtsdatum", moment(daten.Geburtsdatum).format("L"))
        out("Geburtsort", daten.Geburtsort)
        out("PLZ Wohnort", daten.PLZ)
        out("Wohnort", daten.Wohnort)
        out("Straße", daten.Straße)
        out("Hausnummer", daten.Hausnummer)
        out("Email", daten.Email)
        out("Telefonnummer", daten.Telefonnummer)
        out("Handynummer", daten.Handynummer)
        out("Religion", daten.Religion)
        out("Staatsangehörigkeit", daten.Staatsangehörigkeit)
        out("Einwanderer", daten.Einwanderer, true)
        out("Einwanderer_seit", moment(daten.Einwanderer_seit).format("L"))
        out("Familiensprache", daten.Familiensprache)
        out("vorherige Straße", daten.Straße_vorher)
        out("vorheriger Abschluss", daten.Abschluss_vorher)
        out("Eheleute", daten.Eheleute, true)
        out("Vater", daten.Vater_Vorname + " " + daten.Vater_Name)
        out("Mutter", daten.Mutter_Vorname + " " + daten.Mutter_Name)
        out("vorherige Schule", daten.Schule_vorher)
        out("Ort der vorherigen Schule", daten.Schule_vorher_Ort)
        out("Schulordnung", daten.Schulordnung)
        out("Ansprechpartner 1", daten.Ansprechpartner_1_Vorname + " " + daten.Ansprechpartner_1_Vorname)
        out("Ansprechpartner 1 Telefonnummer", daten.Ansprechpartner_1_Telefonnummer)
        out("Ansprechpartner 2", daten.Ansprechpartner_2_Vorname + " " + daten.Ansprechpartner_2_Vorname)
        out("Ansprechpartner 2 Telefonnummer", daten.Ansprechpartner_2_Telefonnummer)
        out("Formular 1 abgegeben", daten.Formular1, true)
        out("Formular 2 abgegeben", daten.Formular2, true)
        out("Formular 3 abgegeben", daten.Formular3, true)
        out("Formular 4 abgegeben", daten.Formular4, true)
        out("Formular 5 abgegeben", daten.Formular5, true)
        out("Ausbildungsbetrieb", daten.Ausbildungsbetrieb_Name)
        out("Ausbildungsbetrieb PLZ", daten.Ausbildungsbetrieb_PLZ)
        out("Ausbildungsbetrieb Ort", daten.Ausbildungsbetrieb_Ort)
        out("Ausbildungsbetrieb Straße", daten.Ausbildungsbetrieb_Straße)
        out("Ausbildungsbetrieb Hausnummer", daten.Ausbildungsbetrieb_Hausnummer)
        out("Ausbildungsbetrieb EMail", daten.Ausbildungsbetrieb_EMail)
        out("Ausbildungsbetrieb Telefonnummer", daten.Ausbildungsbetrieb_Telefonnummer)
        out("Ansprechpartner", daten.Ansprechpartner)
        out("Ausbildungsberuf", daten.Ausbildungsberuf_Name)
        out("Fachrichtung", daten.Fachrichtung)
        //out("", daten.)

        doc.pipe(res)
        doc.fontSize(25)
            .font('Lato-Light.ttf', 25)
            .text('Stammdaten', 100, 60);
        doc.text('', 100, 100)
            .font('Lato-Light.ttf', 10)
            .text(linkeSpalte)
        doc.text('',330, 100)
            .font('Lato-Light.ttf', 10)
            .text(rechteSpalte)
        doc.end();
    });
});
router.get('/backup', function(req, res, next) {
    var spawn    = require('child_process').exec;
    var config   = require('../config');

	//var s3 = upload({ Key: 'mysql-backup-' + moment().format('YYYY-MM-DD-HH-mm-ss') + '.sql' });
    var params = ''
        + " --port=" + config.db.port
        + " --host="+ config.db.host
        + " -u " + config.db.user
        + " --password=" + config.db.passwort
        + " --default-character-set=" + config.db.zeichensatz
        + " " + config.db.name
    console.log(params)
	var mysqldump = spawn(config.db.mysqllocation + "/mysqldump" + params, function (error, stdout, stderr) {
        if (error) {
            console.log(error.stack);
            console.log('Error code: '+error.code);
            console.log('Signal received: '+error.signal);
        }
        res.setHeader('Content-disposition', 'attachment; filename=backup.sql');
        res.setHeader('Content-Type', 'application/octet-stream');
        res.setHeader('charset', config.db.zeichensatz);
        res.send(stdout);
        //console.log('Child Process STDOUT: '+stdout);
        console.log('Child Process STDERR: '+stderr);
    });
});
router.get("/lehrer_hinzufuegen", function(req, res, next) {
    if(req.session.typ != UserType.admin) {
        res.render('verboten', {
            title: 'Zugriff verboten',
        });
        return;
    }
    connection.query("SELECT * FROM klasse", function(err, klasse, fields) {
        if (err) throw(err);
        res.render('admin/lehrer_hinzufügen', {
            klasse: klasse,
            title: 'Lehrer',
        });
    });
});

router.post("/lehrer_hinzufuegen/abschicken", function(req, res, next) {
    var lehrer = {
        EMail: req.body.Lehrer_EMail,
        PasswortHash: sha1(req.body.Lehrer_PasswortHash),
        Vorname: req.body.Lehrer_Vorname,
        Nachname: req.body.Lehrer_Nachname,
        Anrede: req.body.Lehrer_Anrede,
        Geburtsdatum: req.body.Lehrer_Geburtsdatum,
        Telefonnummer: req.body.Lehrer_Telefonnummer,
        Handynummer: req.body.Lehrer_Handynummer,
        Wohnort: req.body.Lehrer_wohnort,
        PLZ: req.body.Lehrer_plz,
        Straße: req.body.Lehrer_straße,
        Hausnummer: req.body.Lehrer_hausnummer,
    }
    var klasse = {
        Klassenlehrer_ID: null,
    }

    connection.query("INSERT INTO lehrer SET ?",[lehrer], function(err, result) {
        if(err) throw err;
        Klassenlehrer_ID = result.insertId;
        connection.query("UPDATE klasse SET ? WHERE ID=?", [klasse, req.body.Klasse_Name], function(err, result) {
            if(err) {
                req.flash("error", "Fehlermeldung");
                res.redirect("/lehrer_hinzufuegen");
            }
            else {
                res.redirect("/lehrer_hinzufuegen");
            }
        });
    });
});

router.get('/benachrichtigung', function(req, res, next) {
    if(sess.typ == UserType.schüler)
    connection.query('SELECT *, lehrer.Vorname AS Lehrer_Vorname, lehrer.ID AS LehrerID FROM schüler INNER JOIN ausbildungsbetrieb ON schüler.Ausbildungsbetrieb_ID = ausbildungsbetrieb.ID INNER JOIN ausbildungsberuf ON schüler.Ausbildungsberuf_ID = ausbildungsberuf.Ausbildungsberuf_ID INNER JOIN klasse ON schüler.Klasse_ID = klasse.ID INNER JOIN lehrer on klasse.Klassenlehrer_ID = Lehrer.ID WHERE schüler.ID=?', [req.session.SchülerID], function(err, rows, fields) {
            if (err) throw err;
            connection.query("SELECT * FROM klasse", function(err, klasse, fields) {
                    if (err) throw(err);
                    connection.query("SELECT * FROM ausbildungsberuf", function(err, ausbildungsberufe, fields) {
                        if (err) throw(err);
                        res.render('admin/admin_benachrichtigung', {
                            title: 'Benachrichtigung',
                            daten: rows[0],
                            klasse: klasse,
                            ausbildungsberufe: ausbildungsberufe,
                    });
                });
            });
        });
});

router.post("/admin/nachricht/abschicken", function(req, res, next) {
    var benachrichtigung = require("./controller/benachrichtigung_admin")
    benachrichtigung.add("", req.session.SchülerID, req.body.Anfrage);
    req.flash("error", "Nachricht erfolgreicht verschickt!")
    res.redirect("/dashboard");
});

router.get("/admin/nachricht/:id", function (req, res, next) {
    var benachrichtigung = require("./controller/benachrichtigung_admin")
    benachrichtigung.getNachricht(req.params.id, function (nachricht) {
        //res.json(nachricht2);
        console.log(nachricht);
        res.render('admin/admin_benachrichtigungen_empfang', {
            title: 'Benachrichtigung',
            nachricht: nachricht,
        });
    });
});

router.get("/admin/nachricht/:id/delete", function(req, res, next) {
    connection.query("DELETE FROM benachrichtigungen_admin WHERE ID=?",[req.params.id], function(err, rows, fields) {
        if (err) throw err;
        res.redirect("/dashboard")
    });
});
// --------------------------------------------
// Sonstiges
// --------------------------------------------

router.get('/passwort-vergessen', function(req, res, next) {
    res.render('passwort-vergessen', {
        title: 'Passwort vergessen',
    });
});
function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
router.post('/passwort-vergessen/abschicken', function(req, res, next) {
    var pwd = makeid()
    console.log(pwd)
    console.log(pwd)
    var hash = require("sha1")
    var neu = {
        PasswortHash: hash(pwd),
    };
    connection.query("SELECT * from schüler WHERE aktiv=1 AND verifiziert=1 AND Email=?",[req.body.email], function(err, schüler, fields) {
        if (err) throw err;
        if (typeof schüler[0] === "undefined") {
            // Jetzt nach Lehrern suchen
            connection.query("SELECT * from lehrer WHERE aktiv=1 AND Email=?",[req.body.email], function(err, lehrer, fields) {
                if (err) throw err;
                if (typeof lehrer[0] === "undefined") {
                    req.flash("error", "Benutzer mit dieser EMail gibt es nicht!")
                    res.redirect("/passwort-vergessen");
                    return;
                } else {
                    var test = connection.query("UPDATE lehrer SET ? WHERE ID=?",[neu, lehrer[0].ID],function(err, rows, fields) {
                        if (err) throw err;
                    });
                    console.log(test.sql)
                    req.flash("error", "Neues Passwort für Lehrer: " + pwd)
                    res.redirect("/passwort-vergessen");
                    return;
                }
            });
        } else {
            var test = connection.query("UPDATE schüler SET ? WHERE ID=?",[neu, schüler[0].ID], function(err, rows, fields) {
                if (err) throw err;
            });
            console.log(test.sql)
            req.flash("error", "Neues Passwort für Schüler: " + pwd)
            res.redirect("/passwort-vergessen");
            return;
        }
    });
});

router.get("/logout", function(req, res, next) {
    req.session.typ = UserType.anonym;
    req.session.name = null;
    req.session.SchülerID = null;
    req.session.LehrerID = null;
    res.redirect("/")
});

router.get('/ludwig-geissler-schule', function(req, res, next) {
    res.render('ludwig-geissler-schule', {
        title: 'Über die Schule',
    });
});
router.get('/info', function(req, res, next) {
    res.render('website', {
        title: 'Über diese Website',
    });
});
router.get('/impressum', function(req, res, next) {
    res.render('impressum', {
        title: 'Impressum',
    });
});
router.get('/javascripterror', function(req, res, next) {
    res.render('javascripterror', {
        title: 'Fehler',
    });
});


module.exports = router;
