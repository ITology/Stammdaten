var config = require('../config');

// Mit MySQL-DB verbinden
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : config.db.host,
  port     : config.db.port,
  user     : config.db.user,
  password : config.db.passwort,
  charset  : config.db.zeichensatz,
});

connection.connect(function(err) {
    if(err) {
        console.error("Aufbau zur MySQL-Datenbank fehlgeschlagen!");
    } else {
        connection.query("USE " + config.db.name + ";");
        console.info("Aufbau zur MySQL-Datenbank erfolgreich!");
    }
});

module.exports = connection;
