var moment = require("moment");
var connection = require("../db.js");

exports.deleteAll = function() {
    connection.query("DELETE FROM benachrichtigungen", function(err, rows, fields) {
        if(err) throw err;
    });
};

module.exports.get = function(callback) {
    connection.query('SELECT *,UNIX_TIMESTAMP(Datum) AS Datum_Unix FROM formular', function(err, rows, fields) {
        if (err) throw err;
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            var d = moment.unix(row.Datum_Unix)
            row["DatumFormat"] = d.format('LLL');
        }
        callback(rows);
    });
};
