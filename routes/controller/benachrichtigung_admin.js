
var connection = require("../db.js");

exports.deleteAll = function() {
    connection.query("DELETE FROM benachrichtigungen_admin", function(err, rows, fields) {
        if(err) throw err;
    });
};

exports.get = function(callback) {
    connection.query("SELECT benachrichtigungen_admin.ID, benachrichtigungen_admin.Lehrer_ID, benachrichtigungen_admin.Schüler_ID, benachrichtigungen_admin.Nachricht, benachrichtigungen_admin.Zeitstempel, schüler.Vorname, schüler.Nachname FROM benachrichtigungen_admin INNER JOIN schüler ON schüler.ID = benachrichtigungen_admin.Schüler_ID", function(err, rows, fields) {
        //if(err) throw err;
        callback(rows);
    });
};
exports.getNachricht = function(ID, callback) {
    connection.query("SELECT benachrichtigungen_admin.ID, benachrichtigungen_admin.Lehrer_ID, benachrichtigungen_admin.Schüler_ID, benachrichtigungen_admin.Nachricht, benachrichtigungen_admin.Zeitstempel, schüler.Vorname, schüler.Nachname FROM benachrichtigungen_admin INNER JOIN schüler ON schüler.ID = benachrichtigungen_admin.Schüler_ID WHERE benachrichtigungen_admin.ID=?",[ID], function(err, rows, fields) {
        //if(err) throw err
        if (typeof rows === "undefined") {
            callback([]);
        } else {
            callback(rows[0]);
        }
    });
};

exports.add = function(Lehrer_ID, Schüler_ID, Nachricht) {
    var daten = {
        Lehrer_ID: Lehrer_ID,
        Schüler_ID: Schüler_ID,
        Nachricht: Nachricht,
    }
    var test = connection.query("INSERT INTO benachrichtigungen_admin SET ?, Zeitstempel=NOW()",[daten], function(err, resut) {
        if(err) throw err;
    });
    console.log(test.sql);
};
exports.test = function(id) {
    return id;
};
