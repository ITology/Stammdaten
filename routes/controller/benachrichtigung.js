
var connection = require("../db.js");

exports.deleteAll = function() {
    connection.query("DELETE FROM benachrichtigungen", function(err, rows, fields) {
        if(err) throw err;
    });
};

exports.get = function(Lehrer_ID, callback) {
    connection.query("SELECT benachrichtigungen.ID, benachrichtigungen.Lehrer_ID, benachrichtigungen.Schüler_ID, benachrichtigungen.Nachricht, benachrichtigungen.Zeitstempel, schüler.Vorname, schüler.Nachname "
        + "FROM benachrichtigungen "
        + "INNER JOIN schüler ON schüler.ID = benachrichtigungen.Schüler_ID "
        + "WHERE Lehrer_ID=?",[Lehrer_ID], function(err, rows, fields) {
        //if(err) throw err;
        callback(rows);
    });
};
exports.getNachricht = function(ID, callback) {
    connection.query("SELECT benachrichtigungen.ID, benachrichtigungen.Lehrer_ID, benachrichtigungen.Schüler_ID, benachrichtigungen.Nachricht, benachrichtigungen.Zeitstempel, schüler.Vorname, schüler.Nachname "
        + "FROM benachrichtigungen "
        + "INNER JOIN schüler ON schüler.ID = benachrichtigungen.Schüler_ID "
        + "WHERE benachrichtigungen.ID=?",[ID], function(err, rows, fields) {
        //if(err) throw err
        if (typeof rows === "undefined") {
            callback([]);
        } else {
            callback(rows[0]);
        }
    });
};

exports.add = function(Lehrer_ID, Schüler_ID, Nachricht) {
    var daten = {
        Lehrer_ID: Lehrer_ID,
        Schüler_ID: Schüler_ID,
        Nachricht: Nachricht,
    }
    var test = connection.query("INSERT INTO benachrichtigungen SET ?, Zeitstempel=NOW()",[daten], function(err, resut) {
        if(err) throw err;
    });
    console.log(test.sql);
};
exports.test = function(id) {
    return id;
};
