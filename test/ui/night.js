module.exports = {
  'Schüler' : function (browser) {
    browser
      .url('http://localhost:3000/')
      .waitForElementVisible('.container', 1000)
      .url('http://localhost:3000/logout')
      .waitForElementVisible('.container', 1000)
      .url('http://localhost:3000/')
      .waitForElementVisible('.container', 1000)
      .url("http://localhost:3000/login")
      .waitForElementVisible('.container', 1000)
      .setValue("input[name='email']", "simon.kundrat@gmail.com")
      .setValue("input[name='passwort']", "1234")
      .click("input[name='submit']")
      .url("http://localhost:3000/kontakt")
      .waitForElementVisible('.container', 1000)
      .url("http://localhost:3000/schueler/stammdaten")
      .waitForElementVisible('.container', 2000)
      .assert.visible(".container")
      .url("http://localhost:3000/schueler/formulare")
      .waitForElementVisible('.container', 2000)
      //.containsText("tr th", "Vorname")
      .end()
  },
  'Registrierung' : function (browser) {
    browser
      .url('http://localhost:3000/registrieren')
      .waitForElementVisible('.container', 1000)
      .setValue("input[name='Email']", "simon.kundrat@gmail.com")
      .setValue("input[name='Anrede']", "Herr")
      //.containsText("tr th", "Vorname")
      .end()
  },
};
