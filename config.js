var config = {}

config.admin = {};
config.db = {};

// Administrator Name und Passwort
config.admin.name = "admin";
config.admin.passwort = "1234";

// MySQL Zugangsdaten
config.db.host = "127.0.0.1";
config.db.port = 3306;
config.db.user = "root";
config.db.passwort = "";
config.db.zeichensatz = "utf8";
config.db.name = "stammdaten";
config.db.mysqllocation = "C:/xampp/mysql/bin";

module.exports = config;
